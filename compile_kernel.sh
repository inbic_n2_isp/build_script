#!/bin/sh

# kernel envirment setting
export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-
export PATH=/opt/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/:$PATH

# make modules -j8

if [ "k" == "$1" ]
then
	echo "kernel $1"
	read -p "Press any key to resume ..."
	make -j8
elif [ "m" == "$1" ]
then
	echo "module $1"
	read -p "Press any key to resume ..."
	make modules -j8
elif [ "mi" == "$1" ]
then
        echo "module install $1"
	if [ $# == 2 ]
	then
		read -p "Press any key to resume ..."
        	make modules_install ARCH=arm64 INSTALL_MOD_PATH=$2
	else
		echo "argument is not 2"
		exit
	fi	
else
	echo "all"
	read -p "Press any key to resume ..."
	make -j8
fi
